# Rust Math Interpreter
[![Pipeline](https://gitlab.com/CoolDeveloper101/rust-math-interpreter/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/CoolDeveloper101/rust-math-interpreter/-/pipelines)

A simple math interpreter written in rust. Super WIP!

## Work Halted!
All work is halted while I learn more about the Rust Programming Language.

**Note: This is an effort to teach myself about the Rust Programming Language and may not contain the best code.**
