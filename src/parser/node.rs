pub enum Node
{
    NumberNode(f32),
    PlusNode(Box<Node>),
    MinusNode(Box<Node>),
    AddNode(Box<Node>, Box<Node>),
    SubtractNode(Box<Node>, Box<Node>),
    MultiplyNode(Box<Node>, Box<Node>),
    DivideNode(Box<Node>, Box<Node>),
    PowerNode(Box<Node>, Box<Node>),
    EmptyNode,
}

impl fmt::Debug for Node{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self{
            Node::NumberNode(v) => write!(f, "({})", v),
            Node::PlusNode(child) => write!(f, "({:?})", child),
            Node::MinusNode(child) => write!(f, "({:?})", child),
            Node::AddNode(child1, child2) => write!(f, "({:?} + {:?})", child1, child2),
            Node::SubtractNode(child1, child2) => write!(f, "({:?} - {:?})", child1, child2),
            Node::MultiplyNode(child1, child2) => write!(f, "({:?} * {:?})", child1, child2),
            Node::DivideNode(child1, child2) => write!(f, "({:?} / {:?})", child1, child2),
            Node::PowerNode(child1, child2) => write!(f, "({:?} ** {:?})", child1, child2),
            _ => write!(f, "()")
        }
    }
}