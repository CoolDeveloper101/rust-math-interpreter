mod lexer;

use std::io::{self, Write};
use lexer::Lexer;

fn main() -> io::Result<()> {
    loop {
	    print!(">>> ");
        io::stdout().flush().unwrap();
        let mut input = String::new();

        io::stdin().read_line(&mut input)?;
        let mut lexer = Lexer::new(input);

        let tokens = lexer.get_tokens().expect("Expected a list of tokens.");

        println!("{:?}", tokens);
    }
    Ok(())
}