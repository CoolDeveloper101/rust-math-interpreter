#[derive(Debug, Clone, Copy)]
pub enum Token{
	NUMBER(f32),
	PLUS,
	MINUS,
	MULTIPLY,
	DIVIDE,
	POWER,
	LPAREN,
	RPAREN,
	EOF
}