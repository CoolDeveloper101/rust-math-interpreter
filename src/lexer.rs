mod token;

use crate::lexer::token::Token;

pub struct Lexer{
	text: String,
	index: usize,
	whitespace: String,
	digits: String,
}

impl Lexer{
	pub fn new(text: String) -> Lexer{
		Lexer{
			text: text,
			index: 0,
			whitespace: " \t\n\r".to_string(),
			digits: "0123456789".to_string()
		}
	}

	fn advance(&mut self){
		self.index += 1;
	}

	fn current(&self) -> char{
		match self.text.chars().nth(self.index){
			Some(c) => c,
			None => '\0'
		}
	}

	pub fn get_tokens(&mut self) -> Result<Vec<Token>, String>{
		let mut tokens: Vec<Token> = Vec::new();
		while self.current() != '\0' {
			if self.whitespace.contains(self.current()) {
				self.advance();
			}
			else if self.digits.contains(self.current()) || self.current() == '.' {
				tokens.push(self.generate_number());
			}
			else if self.current() == '+' {
				self.advance();
				tokens.push(Token::PLUS);
			}
			else if self.current() == '-' {
				self.advance();
				tokens.push(Token::MINUS);
			}
			else if self.current() == '*' {
				self.advance();
				if self.current() == '*' {
					self.advance();
					tokens.push(Token::POWER);
				}
				else {
					tokens.push(Token::MULTIPLY);
				}
			}
			else if self.current() == '/' {
				self.advance();
				tokens.push(Token::DIVIDE);
			}
			else if self.current() == '(' {
				self.advance();
				tokens.push(Token::LPAREN);
			}
			else if self.current() == ')' {
				self.advance();
				tokens.push(Token::RPAREN);
			}
			else {
				let mut err_message = String::from("Unknown character ");
				err_message.push(self.current());
				return Err(err_message);
			}
		}
		Ok(tokens)
	}

	fn generate_number(&mut self) -> Token{
		let mut decimal_point_count = 0;
		if self.current() == '.' {
			decimal_point_count += 1;
		}
		let mut number = String::new();
		number.push(self.current());
		self.advance();

		while self.current() != '\0' && (self.digits.contains(self.current()) || self.current() == '.') {
			if self.current() == '.' {
				decimal_point_count += 1;
				if decimal_point_count > 1 {
					break;
				}
			}

			number.push(self.current());
			self.advance();
		}

		if number.starts_with(".")
		{
				number.insert(0, '0');
		}
		if number.ends_with(".")
		{
			number.push('0');
		}

		let num: f32 = number.parse().unwrap();

		Token::NUMBER(num)
	}
}
